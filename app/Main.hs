{-# LANGUAGE OverloadedStrings #-}
import System.Random
import System.Environment
import Data.Maybe
import qualified Data.Set as Set
import System.IO
import Database.Redis
import Control.Monad.IO.Class
import qualified Data.ByteString.Lazy as B
import Data.ByteString.Char8 (pack, unpack)
import Data.List
import Text.Read
import qualified Data.Map as Map

prefix_genesis_list = 2022

data Coin
    = Genesis Int
    | Mutation Coin Int
    deriving (Show, Read)

type Hash = Int
type Claim = (Hash, Hash) -- (Hash of the current coin, Hash of the previous coin)

scramble :: Int -> Hash
scramble = fst . random . mkStdGen

mint :: StdGen -> (Coin, StdGen)
mint gen =
    let (value, gen') = random gen
    in (Genesis value, gen')


mutate :: StdGen -> Coin -> (Coin, StdGen)
mutate gen coin =
    let (salt, gen') = random gen
    in (Mutation coin salt, gen')

receive :: StdGen -> Set.Set Hash -> Map.Map Hash Hash -> Coin -> Maybe (Coin, Claim, StdGen)
receive gen minted claims coin =
    if verify minted claims coin
       then Just $ let (coin', gen') = mutate gen coin
                   in (coin', (digest coin, digest coin'), gen')
       else Nothing


-- verify that a coin has not been claimed before
-- and verify the history of the coin
--
--
verify minted claims coin =
    not (Map.member (digest coin) claims)
    && verifyHistory minted claims coin

-- verifyHistory
--     if coin is genesis
--     then is minted?
--     else
--       (salt, prev) = coin
--       claim = findClaim(hash prev)
--       if claim.current == hash coin
--       then verify prev
--       else False
verifyHistory :: Set.Set Hash -> Map.Map Hash Hash -> Coin -> Bool
verifyHistory minted claims coin@(Genesis x) = scramble (x + prefix_genesis_list) `Set.member` minted
verifyHistory minted claims coin@(Mutation prev salt) =
    case Map.lookup (digest prev) claims of
         Just current ->
            if current == digest coin
               then verifyHistory minted claims prev
               else False
         Nothing -> False

digest :: Coin -> Hash
digest (Genesis x) = scramble x
digest (Mutation coin x) = scramble $ digest coin + scramble x


-- I/O

readClaims :: IO (Map.Map Hash Hash)
readClaims = do

    claimsPath <- getEnvWithDefault "claims" "CLAIMS"

    contents <- withFile claimsPath ReadMode (\h -> hGetContents' h)

    let fn acc claim = if Map.member (fst claim) acc
                        then acc
                        else Map.insert (fst claim) (snd claim) acc
    return . foldl fn Map.empty . map read $ lines contents

addClaim :: Claim -> IO ()
addClaim claim = do

    claimsPath <- getEnvWithDefault "claims" "CLAIMS"

    appendFile claimsPath . (++"\n") $ show claim

readMinted :: IO (Set.Set Hash)
readMinted = do

    mintedPath <- getEnvWithDefault "minted" "MINTED"

    contents <- withFile mintedPath ReadMode (\h -> hGetContents' h)

    return . Set.fromList . map read $ lines contents

addMinted :: Hash -> IO ()
addMinted hash = do

    mintedPath <- getEnvWithDefault "minted" "MINTED"

    appendFile mintedPath . (++"\n") $ show hash

getEnvWithDefault :: String -> String -> IO String
getEnvWithDefault def env = do

    c <- lookupEnv env

    return $ fromMaybe def c

main = do

    redis <- checkedConnect defaultConnectInfo

    gen <- initStdGen

    args <- getArgs

    case args of
         ("mint":_) -> do
            let ((Genesis x), gen') = mint gen

            addMinted . scramble $ x + prefix_genesis_list

            putStrLn . show $ Genesis x

         ("receive":_) -> do

            contents <- getContents

            minted <- readMinted

            claims <- readClaims

            let coin = read contents
                result = receive gen minted claims coin

            case result of
                Just (coin', claim, gen') -> do

                    print coin'

                    addClaim claim

                Nothing ->
                    putStrLn "ERROR: Cannot receive coin"

         _ -> putStrLn "Help: mint, receive"

    disconnect redis

