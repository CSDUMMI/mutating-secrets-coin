# mutating-secrets-coin

Mutating Secrets Coin PoC Implementation

## Usage
Usage:

- mint generates a new coin
- receive expects a coin from stdin and prints the mutated coin to stdout and publishes the claim on redis.

## minted and claims files
For this PoC to work properly two files must exist locally:
- `minted`
- `claims`

Both of these files must be append-only for all users of the mutating secrets system.

To change them to append-only on Linux:
```sh
$ touch minted claims
$ chattr -e minted claims
$ chattr +a minted claims
```

## Security
This system relies on the security of the operating system and assumes a multi-user setting.

If the minted and claims are append-only, the reading of those files
should make it impossible
